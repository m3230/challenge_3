# Python3 code to demonstrate working of
# Extract values of Particular Key in Nested Values
# Using list comprehension

from dictionary import my_dict

# initializing dictionary
my_dict = {'BMW' : {"red" : 7, "blue" : 9, "white" : 12},
			'Audi' : {"red" : 4, "blue" : 8, "white" : 6},
			'Benz' :{"red" : 9, "blue" : 2, "white" : 5}}

# printing original dictionary
print("The original dictionary is : " + str(my_dict))

# initializing key
temp = "white"

# using item() to extract key value pair as whole
res = [val[temp] for key, val in my_dict.items() if temp in val]

# printing result
print("The extracted values : " + str(res))

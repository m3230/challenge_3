# challenge_3

Extract values from a function that being passed in the object and key

**Programming Language - Python**

## Problem

Extract no. of white cars from all brands in a garage

## Solution

Nested number of cars from all brands as a key and extract particular key (colour) as values

## Expected Output

Output = [all white cars]


